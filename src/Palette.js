//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Palette.js                                                    //
//  Project   : color_grid                                                    //
//  Date      : Aug 27, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt 2019, 2020                                            //
//                                                                            //
//  Description :                                                             //
//   Manages the current color scheme of the game.                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Palette                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
const PALETTE_INVALID_COLOR_INDEX = -1;

//------------------------------------------------------------------------------
class Palette
{
    //--------------------------------------------------------------------------
    constructor(colorsCount)
    {
        this.colors      = [];
        this.colorsCount = colorsCount;

        // for(let i = 0; i < colorsCount; ++i) {
        //     let c = chroma.hsl(360 / colorsCount * i, 1, 0.5);
        //     this.colors.push(c);
        // }

        this._getPaletteColors();
        this.colors.push(chroma("white"));


        this.backgroundColor = chroma("#002A37");
        // this.backgroundColor = chroma("#FEF7E1");
    } // ctor

    getBackgroundColor()
    {
        return this.backgroundColor;
    }

    //--------------------------------------------------------------------------
    getRandomIndex()
    {
        return Random_Int(0, this.colorsCount);
    }

    //--------------------------------------------------------------------------
    getDefeatIndex()
    {
        return this.colors.length-1;
    }

    //--------------------------------------------------------------------------
    getColor(i)
    {
        return this.colors[i];
    } // getColor


    //--------------------------------------------------------------------------
    _getPaletteColors()
    {
        let palettes = [[
           "#D93f99",
           "#EB0003",
           "#E40080",
           "#7068CA",
           "#0087D9",
           "#00A59A",
           "#7E9F00",
        ]];

        let pal_index = Random_Int(0, palettes.length);
        let colors    = palettes[pal_index];
        for(let i = 0; i < colors.length; ++i) {
            let row   = colors[i];
            let color = chroma(row);
            this.colors.push(color);
        }

        this.colors.sort(() => Math.random() - 0.5);
    }
}; // class Palette
